var Shop = {

    namespace: 'meineShopItems',

    init: function() {
        this.initButtonListener();
        this.loadAsList();

        parseInt("12")
    },

    setItem: function( item ) {
        var items = this.getItem();

        var found = false;
        for(var i = 0; i < items.length; i++) {
            var storedItem = items[i];
            if(item.title === storedItem.title) {
                items[i] = item;
                console.log('item replaced');
                found = true;
            }
        }

        if(!found) {
            items.push(item); 
        }

        localStorage.setItem(this.namespace, JSON.stringify(items));
    },

    /**
     * @return array
     */
    getItem: function() {
        var storage = localStorage.getItem(this.namespace);
        if(storage === null) {
            storage = "[]";
        }
        return JSON.parse(storage);
    },

    /**
     * reacts to buttons like <btn class="addtoshop" title="Drohne" price="12.000" image="/myimage.png"> Zum Warenkorb hinzufügen </btn>
     */
    initButtonListener: function() {
        $('button.addtoshop').each(function() {
            $(this).on('click', function() {

                var title = $(this).attr('title');
                var price = $(this).attr('price');
                var img = $(this).attr('img');

                Shop.setItem({
                    title: title,
                    price: price,
                    image: img
                });
            });
        });
    },

    loadAsList: function() {
        var items = this.getItem();
        for(var i = 0; i < items.length; i++) {
            var item = items[i];
            $('#modal-content').append('<p>'+item.title+item.price+'<p>');
        }
    }

};


(function() {
    document.addEventListener("DOMContentLoaded", function() {
        Shop.init();
      });
}());